# k8s-conf

This repository contains Kubernetes configuration files for cloud-compatible services in the Radiodiodi organization. These include but are not limited to: the web application, databases, and a reverse proxy/lb.

The plan is to write a configuration that is designed to work both in development in a local single-node Kubernetes cluster, as well as in a cloud environment (Google Cloud, DigitalOcean, AWS, etc.). Currently, the configuration is written for the local cluster.

Since some resources differ greatly between these two environments (such as volumes and ingress networking), some changes need to be done when deploying the resources to the cloud.

## How to install

Set up a local Kubernetes cluster. We recommend [Docker for Mac Kubernetes](https://docs.docker.com/docker-for-mac/kubernetes/) (MacOS) or [MicroK8s](https://microk8s.io/) (all platforms). Install [kubectl](https://kubernetes.io/docs/tasks/tools/install-kubectl/). Set up a working connection.

The volumes used are _host path volumes_. The volumes reserve space from the host machine at `/tmp/.kubevolumes`. If you bump into an issue where a volume can't bind to a host path, try creating that directory under `/tmp/.kubevolumes` manually and recreating the volume.

To set up all resources, run

    kubectl apply -f .

in the project root. This will read all `yaml` config files and create the defined resources (similar to `docker-compose up` in a sense).

To remove all resources, run

    kubectl delete -f .

in the project root. This is similar to `docker-compose down`. Volumes or their contents will not _really_ be deleted, since to actually remove volumes you need to [jump through some hoops](https://github.com/kubernetes/kubernetes/issues/77258#issuecomment-502209800).

## Kubernetes Dashboard

### Authentication token

To log in to the Kubernetes Dashboard, you can use a service account token as a password of sorts. To find out the token, run the following commands. Copy the output for the next step.

    TOKEN_NAME=$(kubectl -n kubernetes-dashboard get serviceaccount kubernetes-dashboard -o jsonpath='{.secrets[0].name}')
    kubectl -n kubernetes-dashboard get secret "${TOKEN_NAME}" -o jsonpath='{.data.token}' | base64 -d; echo

### Accessing Dashboard via proxy

Run the following command to activate a proxy connection to the cluster.

    kubectl proxy

The Dashboard is now accessible at http://localhost:8001/api/v1/namespaces/kubernetes-dashboard/services/https:kubernetes-dashboard:/proxy/#/login.

Select the token authentication method. Copy the token from the previous step to the field and log in. It is recommended to store this token in a password manager for easier access later.

## Caddy Ingress

The cluster has a [Caddy](https://caddyserver.com/) instance working as the Ingress load balancer and reverse proxy. You can find Caddy through _node port networking_ at [localhost:30100](http://localhost:30100).

Since Caddy routes traffic based on the requested DNS name (e.g. radiodiodi.fi), navigating to [localhost:30100](http://localhost:30100) just shows a _404 Not Found_ page. To circumvent this during development, you can manipulate your `Host` header using a 3rd party browser addon, such as [ModHeader for Chrome](https://chrome.google.com/webstore/detail/modheader/idgpnmonknjnojddfkpgkljpfnnfcklj). Simply set your `Host` header to read `radiodiodi.fi`, or any other matched DNS name, and refresh.

## Authors

Written by Aarni Halinen & Jan Tuomi, 2019.
